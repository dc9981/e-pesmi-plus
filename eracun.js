//Priprava knjižnic
var formidable = require("formidable");
var util = require('util');

if (!process.env.PORT)
  process.env.PORT = 8080;

// Priprava povezave na podatkovno bazo
var sqlite3 = require('sqlite3').verbose();
var pb = new sqlite3.Database('chinook.sl3');

// Priprava strežnika
var express = require('express');
var expressSession = require('express-session');
var streznik = express();
streznik.set('view engine', 'ejs');
streznik.use(express.static('public'));
streznik.use(
  expressSession({
    secret: '1234567890QWERTY', // Skrivni ključ za podpisovanje piškotkov
    saveUninitialized: true,    // Novo sejo shranimo
    resave: false,              // Ne zahtevamo ponovnega shranjevanja
    cookie: {
      maxAge: 3600000           // Seja poteče po 60min neaktivnosti
    }
  })
);

var razmerje_usd_eur = 0.877039116;
var racunID;
var strankaID;
var a;
function davcnaStopnja(izvajalec, zanr) {
  switch (izvajalec) {
    case "Queen": case "Led Zepplin": case "Kiss":
      return 0;
    case "Justin Bieber":
      return 22;
    default:
      break;
  }
  switch (zanr) {
    case "Metal": case "Heavy Metal": case "Easy Listening":
      return 0;
    default:
      return 9.5;
  }
}

// Prikaz seznama pesmi na strani
streznik.get('/', function(zahteva, odgovor) {
  if(!zahteva.session.userID){
    zahteva.session.userID==null;
    odgovor.redirect('/prijava') ;
  } else {
  
  if (zahteva.session.userID ==null){
    odgovor.redirect('/prijava');
  }
  

  pb.all("SELECT Track.TrackId AS id, Track.Name AS pesem, \
          Artist.Name AS izvajalec, Track.UnitPrice * " +
          razmerje_usd_eur + " AS cena, \
          COUNT(InvoiceLine.InvoiceId) AS steviloProdaj, \
          Genre.Name AS zanr \
          FROM Track, Album, Artist, InvoiceLine, Genre \
          WHERE Track.AlbumId = Album.AlbumId AND \
          Artist.ArtistId = Album.ArtistId AND \
          InvoiceLine.TrackId = Track.TrackId AND \
          Track.GenreId = Genre.GenreId \
          GROUP BY Track.TrackId \
          ORDER BY steviloProdaj DESC, pesem ASC \
          LIMIT 100", function(napaka, vrstice) {
    if (napaka)
      odgovor.sendStatus(500);
    else {
        for (var i=0; i<vrstice.length; i++)
          vrstice[i].stopnja = davcnaStopnja(vrstice[i].izvajalec, vrstice[i].zanr);
        odgovor.render('seznam', {seznamPesmi: vrstice});
      }
  })
}
})

// Dodajanje oz. brisanje pesmi iz košarice
streznik.get('/kosarica/:idPesmi', function(zahteva, odgovor) {
  var idPesmi = parseInt(zahteva.params.idPesmi);
  if (!zahteva.session.kosarica)
    zahteva.session.kosarica = [];
  if (zahteva.session.kosarica.indexOf(idPesmi) > -1) {
    zahteva.session.kosarica.splice(zahteva.session.kosarica.indexOf(idPesmi), 1);
  } else {
    zahteva.session.kosarica.push(idPesmi);
  }
  
  odgovor.send(zahteva.session.kosarica);
});

// Vrni podrobnosti pesmi v košarici iz podatkovne baze
var pesmiIzKosarice = function(zahteva, callback) {
  if (!zahteva.session.kosarica || Object.keys(zahteva.session.kosarica).length == 0) {
    callback([]);
  } else {
    pb.all("SELECT Track.TrackId AS stevilkaArtikla, 1 AS kolicina, \
    Track.Name || ' (' || Artist.Name || ')' AS opisArtikla, \
    Track.UnitPrice * " + razmerje_usd_eur + " AS cena, 0 AS popust, \
    Genre.Name AS zanr \
    FROM Track, Album, Artist, Genre \
    WHERE Track.AlbumId = Album.AlbumId AND \
    Artist.ArtistId = Album.ArtistId AND \
    Track.GenreId = Genre.GenreId AND \
    Track.TrackId IN (" + zahteva.session.kosarica.join(",") + ")",
    function(napaka, vrstice) {
      if (napaka) {
        callback(false);
      } else {
        for (var i=0; i<vrstice.length; i++) {
          vrstice[i].stopnja = davcnaStopnja((vrstice[i].opisArtikla.split(' (')[1]).split(')')[0], vrstice[i].zanr);
        }
        callback(vrstice);
      }
    })
  }
}

streznik.get('/kosarica', function(zahteva, odgovor) {
  pesmiIzKosarice(zahteva, function(pesmi) {
    if (!pesmi)
      odgovor.sendStatus(500);
    else
      odgovor.send(pesmi);
  });
})

// Vrni podrobnosti pesmi na računu
var pesmiIzRacuna = function(racunId, callback) {
    pb.all("SELECT Track.TrackId AS stevilkaArtikla, 1 AS kolicina, \
    Track.Name || ' (' || Artist.Name || ')' AS opisArtikla, \
    Track.UnitPrice * " + razmerje_usd_eur + " AS cena, 0 AS popust, \
    Genre.Name AS zanr \
    FROM Track, Album, Artist, Genre \
    WHERE Track.AlbumId = Album.AlbumId AND \
    Artist.ArtistId = Album.ArtistId AND \
    Track.GenreId = Genre.GenreId AND \
    Track.TrackId IN (SELECT InvoiceLine.TrackId FROM InvoiceLine, Invoice \
    WHERE InvoiceLine.InvoiceId = Invoice.InvoiceId AND Invoice.InvoiceId = " + racunId + ")",
    function(napaka, vrstice) {
      if (napaka) {
        callback(false);
      } else {
        for (var i=0; i<vrstice.length; i++) {
          vrstice[i].stopnja = davcnaStopnja((vrstice[i].opisArtikla.split(' (')[1]).split(')')[0], vrstice[i].zanr);
        }
        callback(vrstice);
      }
    })
}

// Vrni podrobnosti o stranki iz računa
var strankaIzRacuna = function(racunId, callback) {
    pb.all("SELECT Customer.* FROM Customer, Invoice \
            WHERE Customer.CustomerId = Invoice.CustomerId AND Invoice.InvoiceId = " + racunId,
    function(napaka, vrstice) {
      
      console.log(vrstice);
      callback(vrstice);
    })
}


var stranka = function(strankaId, callback) {
  //pb.get("SELECT Customer.* FROM Customer WHERE Customer.CustomerId = $cid", {},
  pb.get("SELECT Customer.* FROM Customer WHERE Customer.CustomerId = " + strankaId, {},
    function(napaka, vrstica) {
      console.log(vrstica);
      //a = vrstica;
      callback(vrstica);
  });
}


// Izpis računa v HTML predstavitvi na podlagi podatkov iz baze
streznik.post('/izpisiRacunBaza', function(zahteva, odgovor) {
  //zahteva.seznamRacunov
  //odgovor.redirect('/izpisiRacun/html')
  //odgovor.end();
  //console.log("racunID ="+zahteva.seznamRacunov);
  //strankaIzRacuna(zahteva.seznamRacunov,null);
  
  
  var form = new formidable.IncomingForm();
  
  form.parse(zahteva, function (napaka1, polja, datoteke) {
    //console.log("bbb-"+polja.seznamRacunov);
    //odgovor.redirect('/')
    //strankaIzRacuna(polja.seznamRacunov,null);
    //pesmiIzRacuna(polja.seznamRacunov,null);
    racunID = polja.seznamRacunov;
    //strankaID = polja.seznamStrank;
    odgovor.redirect('/izpisiIzRacuna/html')
  });
})

streznik.get('/izpisiIzRacuna/:oblika', function(zahteva, odgovor) {
  console.log("v IZPIS IZZZZ RACUNA ");
  //var mem;
  /*strankaIzRacuna(racunID,function(vrstice){
    //console.log("vvv-" + vrstice);
    a = vrstice;
    //console.log("aaa-" + a);
  })*/
  
  strankaIzRacuna(racunID,function(vrstice){
    //console.log("vvv-" + vrstice);
    a = vrstice;
    //console.log("aaa-" + a);
  })
  
  
  pesmiIzRacuna(racunID, function(pesmi) {
    if (!pesmi) {
      odgovor.sendStatus(500);
    } else if (pesmi.length == 0) {
      odgovor.send("<p>V košarici nimate nobene pesmi, \
        zato računa ni mogoče pripraviti!</p>");
    } else {
      odgovor.setHeader('content-type', 'text/xml');
      odgovor.render('eslog', {
        vizualiziraj: zahteva.params.oblika == 'html' ? true : false,
        postavkeRacuna: pesmi,
        PodatkiKupca: a
        //strankaIzRacuna: pesmi
      })  ;
    }
  })
})
//-----------------------------------------------
streznik.get('/izpisiRacun/:oblika', function(zahteva, odgovor) {
  //var mem;
  console.log("v IZPIS RACUN!!!!! ");
  
  
  pesmiIzKosarice(zahteva, function(pesmi) {
    if (!pesmi) {
      odgovor.sendStatus(500);
    } else if (pesmi.length == 0) {
      odgovor.send("<p>V košarici nimate nobene pesmi, \
        zato računa ni mogoče pripraviti!</p>");
    } else {
      odgovor.setHeader('content-type', 'text/xml');
      odgovor.render('eslog', {
        vizualiziraj: zahteva.params.oblika == 'html' ? true : false,
        postavkeRacuna: pesmi,
        PodatkiKupca: a
        //strankaIzRacuna: pesmi
      })  ;
    }
  })
})



//--------------------------------



/*
// Izpis računa v HTML predstavitvi ali izvorni XML obliki
streznik.get('/izpisiRacun/:oblika', function(zahteva, odgovor) {
  var b;
  /*
  console.log("strankaId-"+strankaID);
  stranka(strankaID,function(vrst){
    //console.log("vvv-" + vrstice);
    b =  vrst; 
    console.log("bbb1-" + b.FirstName);
    console.log("vvv1-"+vrst.FirstName);
  });
  */
   
    //var y = Object.assign({}, x); 
    /*
  pesmiIzKosarice(zahteva, function(pesmi) {
    //console.log("bbb2222222222s-" + b.FirstName);
    if (!pesmi) {
      odgovor.sendStatus(500);
    } else if (pesmi.length == 0) {
      odgovor.send("<p>V košarici nimate nobene pesmi, \
        zato računa ni mogoče pripraviti!</p>");
    } else {
      odgovor.setHeader('content-type', 'text/xml');
      odgovor.render('eslog', {
        vizualiziraj: zahteva.params.oblika == 'html' ? true : false,
        postavkeRacuna: pesmi,
        PodatkiKupca: b
      })  ;
    }
  })
})
*/


// Privzeto izpiši račun v HTML obliki
streznik.get('/izpisiRacun', function(zahteva, odgovor) {
  odgovor.redirect('/izpisiRacun/html')
})

// Vrni stranke iz podatkovne baze
var vrniStranke = function(callback) {
  pb.all("SELECT * FROM Customer",
    function(napaka, vrstice) {
      callback(napaka, vrstice);
    }
  );
}

// Vrni račune iz podatkovne baze
var vrniRacune = function(callback) {
  pb.all("SELECT Customer.FirstName || ' ' || Customer.LastName || ' (' || Invoice.InvoiceId || ') - ' || date(Invoice.InvoiceDate) AS Naziv, \
          Invoice.InvoiceId \
          FROM Customer, Invoice \
          WHERE Customer.CustomerId = Invoice.CustomerId",
    function(napaka, vrstice) {
      callback(napaka, vrstice);
    }
  );
}

// Registracija novega uporabnika
streznik.post('/prijava', function(zahteva, odgovor) {
  //console.log("v registraciji1..");
  var form = new formidable.IncomingForm();
  //console.log("v registraciji..");
  form.parse(zahteva, function (napaka, polja, datoteke) {
     //console.log("v registraciji.. ");
     //console.log("napaka--- " +polja.Address);
           /*
            pb.run("INSERT INTO Customer (FirstName, LastName, Company, Address, City, State, Country, PostalCode, \
	            Phone, Fax, Email, SupportRepId) VALUES ("+polja.FirstName+","+polja.LastName+","+polja.Company+","+polja.Address+","+polja.City+","+polja.State+","+polja.Country+","+polja.PostalCode+","+polja.Phone+","+polja.Fax+","+polja.Email+","+ 3+")", 
	            {}, function(napaka) {
	              vrniStranke(function(napaka1, stranke) {
                  vrniRacune(function(napaka2, racuni) {
                    odgovor.render('prijava', {sporocilo: "", seznamStrank: stranke, seznamRacunov: racuni});  
                  }) 
                });
      });*/
     
     var bre = true;
     if (polja.FirstName.trim() == "" || polja.LastName.trim() == "" || polja.Company.trim() == "" || polja.Address.trim() == "" || polja.City.trim() == "" || polja.State.trim() == "" || polja.Country.trim() == ""|| polja.PostalCode.trim() == "" || polja.Phone.trim() == "" || polja.Fax.trim() == "" || polja.Email.trim == ""){
       bre = false;
     }
     
     if(bre) {
       pb.run("INSERT INTO Customer (FirstName, LastName, Company, Address, City, State, Country, PostalCode, \
	            Phone, Fax, Email, SupportRepId) VALUES ('"+polja.FirstName+"','"+polja.LastName+"','"+polja.Company+"','"+polja.Address+"','"+polja.City+"','"+polja.State+"','"+polja.Country+"','"+polja.PostalCode+"','"+polja.Phone+"','"+polja.Fax+"','"+polja.Email+"','3')", 
	            {}, function(napaka) {
	              vrniStranke(function(napaka1, stranke) {
                  vrniRacune(function(napaka2, racuni) {
                    odgovor.render('prijava', {sporocilo: "Stranka je bila uspešno registrirana.", seznamStrank: stranke, seznamRacunov: racuni});  
                  }) 
                });
      });
       
       
     } else {
       vrniStranke(function(napaka1, stranke) {
                  vrniRacune(function(napaka2, racuni) {
                    odgovor.render('prijava', {sporocilo: "Prišlo je do napake pri registraciji nove stranke. Prosim preverite vnešene podatke in poskusite znova.", seznamStrank: stranke, seznamRacunov: racuni});  
                  }) 
                });
       
     }
     
     
     
     
     
  })
  
})

// Prikaz strani za prijavo
streznik.get('/prijava', function(zahteva, odgovor) {
  console.log("v prikaz strani za registracijo..");
  vrniStranke(function(napaka1, stranke) {
      vrniRacune(function(napaka2, racuni) {
        odgovor.render('prijava', {sporocilo: "", seznamStrank: stranke, seznamRacunov: racuni});  
      }) 
    });
})

// Prikaz nakupovalne košarice za stranko
streznik.post('/stranka', function(zahteva, odgovor) {
  var form = new formidable.IncomingForm();
  
  form.parse(zahteva, function (napaka1, polja, datoteke) {
//<<<<<<< HEAD
    //console.log("polja-" + polja);
    //console.log("polja.FN-" + polja.seznamStrank);
    //console.log("polja.sef-" + polja.seznamStrank.FirstName);
    //console.log("polja.stf-" + polja.stranka.FirstName);
    //console.log("strnka="+stranka);
    //console.log("strnka.vrst="+stranka.vrstica);
    //zahteva.session.id = "asdasdasdasd";
    //console.log("session="+zahteva.session.id);
    //console.log("bbbb-"+zahteva.session.bre);
    zahteva.session.userID = polja.seznamStrank;
    console.log("usrID-"+ zahteva.session.userID);
    racunID = polja.seznamRacunov;
    strankaID = zahteva.session.userID;
    console.log("racinID1-"+strankaID);
    stranka(strankaID,function(vrstice){
    //console.log("vvv-" + vrstice);
    a = vrstice;
    //console.log("aaa-" + a);
  });
    
    /*
    strankaVAR;
    stranka(strankaID,function(vrst){
      //console.log("vvv-" + vrstice);
      strankaVAR =  vrst; 
      console.log("bbb1-" + strankaVAR.FirstName);
      console.log("vvv1-"+vrst.FirstName);
    });
    */
    
//=======
    //console.log("bbb-"+polja.seznamStrank);
//>>>>>>> racun
    odgovor.redirect('/')
  });
})

// Odjava stranke
streznik.post('/odjava', function(zahteva, odgovor) {
   //console.log("session1="+zahteva.session.id);
    //var a = zahteva.session.id;
    //console.log("log-"+a)
    //zahteva.session.id = null;
    //a = "breee";
    //zahteva.session.bre = "bree";
    
    //console.log("--a-"+a);
    //zahteva.session.id.push(null);
    //console.log("session2="+zahteva.session.id);
    //console.log("&&$$-"+zahteva.session.bre);
    //zahteva.sessionID = null;
    //console.log("session3="+zahteva.sessionID);
    zahteva.session.kosarica = null;
    zahteva.session.userID = null;
    
    odgovor.redirect('/prijava') 
})



streznik.listen(process.env.PORT, function() {
  console.log("Strežnik pognan!");
})
